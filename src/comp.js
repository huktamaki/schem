function tileCounter(array, crit) {
  var counter = 0
  for (var i = 0; i < array.length; i++) {
    for (var j = 0; j < array[i].length; j++) {
      if (array[i][j] === crit) {
        counter++
      }
    }
  }
  return counter
}

function getEmptyNeighbours(list, array) {
  // directions: 0 (right), 1 (left), 2 (bottom), 3 (top)
  var neighbours = []
  for (var i = 0; i < list.length; i++) {
    var posX = list[i][0]
    var posY = list[i][1]
    if (posX > 0) {
      if (isArrayInArray(list, [posX - 1, posY]) === false && array[posX - 1][posY] === 0) {
        neighbours.push([posX - 1, posY, 0])
      }
    }
    if (posX < maxDimension - 1) {
      if (isArrayInArray(list, [posX + 1, posY]) === false && array[posX + 1][posY] === 0) {
        neighbours.push([posX + 1, posY, 1])
      }
    }
    if (posY > 0) {
      if (isArrayInArray(list, [posX, posY - 1]) === false && array[posX][posY - 1] === 0) {
        neighbours.push([posX, posY - 1, 2])
      }
    }
    if (posY < maxDimension - 1) {
      if (isArrayInArray(list, [posX, posY + 1]) === false && array[posX][posY + 1] === 0) {
        neighbours.push([posX, posY + 1, 3])
      }
    }
  }
  neighbours = multiDimensionalUnique(neighbours)
  return neighbours
}

function getTiles(tiles) {
  var list = []
  for (var i = 0; i < tiles.length; i++) {
    for (var j = 0; j < tiles[i].length; j++) {
      if (tiles[i][j] === 1) {
        list.push([i, j])
      }
    }
  }
  return list
}

function multiDimensionalUnique(arr) {
  var uniques = []
  var itemsFound = {}
  for (var i = 0, l = arr.length; i < l; i++) {
    var stringified = JSON.stringify(arr[i])
    if (itemsFound[stringified]) {
      continue
    }
    uniques.push(arr[i])
    itemsFound[stringified] = true
  }
  return uniques
}

function isArrayInArray(arr, item) {
  var item_as_string = JSON.stringify(item)
  var contains = arr.some(function(ele) {
    return JSON.stringify(ele) === item_as_string
  })
  return contains
}

function playerChecker(x, y, wall) {
  var bool
  if (wall[x][y] === 0) {
    bool = true
  } else {
    bool = false
  }
  if (allDeepEqual([getNeighbours(player.x, player.y, tileArray, 0, 0, 0), [true, true, true, true]]) === true && states.tileGrabbed === false) {
    bool = true
  }
  return bool
}

function wallCollision(wall, form) {
  var collided = []
  for (var i = 0; i < form.length; i++) {
    for (var j = 0; j < form[i].length; j++) {
      if (form[i][j] === 1 && wall[i][j] === 0) {
        collided.push([i, j])
      }
    }
  }
  return collided
}

function getVertices(array) {
  var edges = []
  // every vertices
  for (var i = 0; i < array.length; i++) {
    for (var j = 0; j < array[i].length; j++) {
      if (array[i][j] === 1) {
        // upper left edge
        edges.push([i - 0.5, j - 0.5])
        // upper right edge
        edges.push([i + 0.5, j - 0.5])
        // lower left edge
        edges.push([i - 0.5, j + 0.5])
        // lower right edge
        edges.push([i + 0.5, j + 0.5])
      }
    }
  }
  return edges
}

function rearrangeTiles(array, steps) {
  for (var k = 0; k < steps; k++) {
    var tiles = []
    var emptyNeighbours = []
    var pickedTile
    var pickedEmpty
    for (var i = 0; i < array.length; i++) {
      for (var j = 0; j < array[i].length; j++) {
        if (array[i][j] === 1) {
          tiles.push([i, j])
        }
      }
    }
    pickedTile = tiles[Math.floor(Math.random() * tiles.length)]
    for (var i = 0; i < tiles.length; i++) {
      emptyNeighbours.push(getNeighbours(tiles[i][0], tiles[i][1], array, 0, 1, 1))
    }
    emptyNeighbours = emptyNeighbours.flat()
    emptyNeighbours = uniqBy(emptyNeighbours, JSON.stringify)
    var trueEmpties = []
    for (var i = 0; i < emptyNeighbours.length; i++) {
      if (emptyNeighbours[i] !== false) {
        trueEmpties.push(emptyNeighbours[i])
      }
    }
    pickedEmpty = trueEmpties[Math.floor(Math.random() * trueEmpties.length)]
    array[pickedEmpty[0]][pickedEmpty[1]] = 1
    array[pickedTile[0]][pickedTile[1]] = 0
    pickedTile = []
  }
}

function rotate2DArray(array) {
  var result = []
  for (var i = 0; i < array[0].length; i++) {
    var row = array.map(e => e[i]).reverse()
    result.push(row)
  }
  return result
}

function getNeighbours(x, y, array, check, checkType, limit) {
  neighbours = []
  if (x < array.length - 1 - limit) {
    if (array[x + 1][y] === check) {
      if (checkType === 0) {
        neighbours.push(true)
      } else {
        neighbours.push([x + 1, y])
      }
    } else {
      if (checkType === 0) {
        neighbours.push(false)
      }
    }
  } else {
    neighbours.push(false)
  }
  if (x > limit) {
    if (array[x - 1][y] === check) {
      if (checkType === 0) {
        neighbours.push(true)
      } else {
        neighbours.push([x - 1, y])
      }
    } else {
      if (checkType === 0) {
        neighbours.push(false)
      }
    }
  } else {
    neighbours.push(false)
  }
  if (y < array.length - 1 - limit) {
    if (array[x][y + 1] === check) {
      if (checkType === 0) {
        neighbours.push(true)
      } else {
        neighbours.push([x, y + 1])
      }
    } else {
      if (checkType === 0) {
        neighbours.push(false)
      }
    }
  } else {
    neighbours.push(false)
  }
  if (y > limit) {
    if (array[x][y - 1] === check) {
      if (checkType === 0) {
        neighbours.push(true)
      } else {
        neighbours.push([x, y - 1])
      }
    } else {
      if (checkType === 0) {
        neighbours.push(false)
      }
    }
  } else {
    neighbours.push(false)
  }
  return neighbours
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}

function deepCopy(obj) {
  if (typeof obj == 'object') {
    if (isArray(obj)) {
      var l = obj.length
      var r = new Array(l)
      for (var i = 0; i < l; i++) {
        r[i] = deepCopy(obj[i])
      }
      return r
    } else {
      var r = {}
      r.prototype = obj.prototype
      for (var k in obj) {
        r[k] = deepCopy(obj[k])
      }
      return r
    }
  }
  return obj
}

function isArray(obj) {
  if (obj instanceof Array)
    return true
  for (var k in ARRAY_PROPS) {
    if (!(k in obj && typeof obj[k] == ARRAY_PROPS[k]))
      return false
  }
  return true
}

function uniqBy(a, key) {
  var seen = {}
  return a.filter(function(item) {
    var k = key(item)
    return seen.hasOwnProperty(k) ? false : (seen[k] = true)
  })
}

function type(x) {
  return Object.prototype.toString.call(x)
}

function zip(arrays) {
  return arrays[0].map(function(_, i) {
    return arrays.map(function(array) {
      return array[i]
    })
  })
}

function allCompareEqual(array) {
  return array.every(function(x) {
    return x == array[0]
  })
}

function isArr(x) {
  return type(x) == type([])
}

function getLength(x) {
  return x.length
}

function allTrue(array) {
  return array.reduce(function(a, b) {
    return a && b
  }, true)
}

function allDeepEqual(things) {
  if (things.every(isArr))
    return allCompareEqual(things.map(getLength)) &&
      allTrue(zip(things).map(allDeepEqual))
  else
    return allCompareEqual(things)
}
